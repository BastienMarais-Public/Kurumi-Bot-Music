# Kurumi Bot V3

## To use it

Just click on : [invitation link](https://discord.com/api/oauth2/authorize?client_id=887348471030485002&permissions=8&scope=bot%20applications.commands)

## Installation on server

### Setup the projet

On the server that should run the bot type :

```sh
apt install -y curl libssl-dev build-essential git sudo vim
adduser runner
adduser runner sudo
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
nvm install node // v18+

cd /home/runner
su runner
git clone https://gitlab.com/BastienMarais-Public/Kurumi-Bot.git
cd Kurumi-Bot
npm i
```

Create the `./src/config.json` file and fill it in :

```json
{
  "ownerId": "",
  "clientId": "",
  "guildId": [""],
  "token": "",
  "customGifsManager":"",
  "dbLogin": "",
  "dbPassword": "",
  "dbName": "",
  "dbFile": "",
  "animeDomainName": "animecat.net",
  "animeSiteName": "Anime Cat",
  "animeErrorAdminId": "",
  "isProd" : true
}
```

| Key               | description                                                                                                                    |
| :---------------- | :----------------------------------------------------------------------------------------------------------------------------- |
| ownerId           | Id of the bot owner                                                                                                            |
| clientId          | Id of the application                                                                                                          |
| guildId           | Server id list for the dev                                                                                                     |
| token             | Token of the bot found in https://discord.com/developers/applications/                                                         |
| customGifsManager | Url of [gifs-manager back](https://gitlab.com/BastienMarais-Public/gifs-manager/)                                              |
| dbLogin           | Database user name                                                                                                             |
| dbPassword        | Database user's password                                                                                                       |
| dbName            | Name of the database                                                                                                           |
| dbFile            | Name of the database file for example `database.db`.                                                                           |
| animeDomainName   | Domain name Neko-sama/Anime Cat (if it changes, it's easier to modify the config than the code)                                |
| animeSiteName     | Site name Neko-sama/Anime Cat (if it changes, it's easier to modify the config than the code)                                  |
| animeErrorAdminId | Id of person who will be notified in case of check-subscribers error                                                           |
| isProd            | Indicates if we are in production or not with a boolean (true or false). This allows to launch the build files and not the dev |

### Launching the bot

#### DEV

```sh
npm run dev
```

#### PROD

```sh
npm run build
npm run start
```

### Migration between V1 and V2

A migration script for statistics and counters is available via the `npm run migrate-v2` command

To use it, simply replace the files `./src/migration/data/stats.json` and `./src/migration/data/counters-ban.json` with the equivalent files in the `/data` folder of V1. Then run the migrate script.

### Migration between V2 and V3

A migration script for statistics is available via the `npm run migrate-v3` command

There's nothing else in particular to do. It will rename the neko-* commands to anime-*.

🚨 I've also added unpublishing of old command names in this script.
