import {
  roleMention,
  SlashCommandBuilder,
  userMention,
} from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Logger, LoggerType } from "../../models/logger/logger";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Allows to remove a role to a person (admin role required)
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("role-remove")
    .setDescription("Allows to remove a role to a person (admin role required)")
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("Person who will lose the role")
        .setRequired(true)
    )
    .addRoleOption((option) =>
      option
        .setName("role")
        .setDescription("Role to remove to the person")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const target = interaction.options.getUser("target");
    const role = interaction.options.getRole("role");
    const user = interaction.user.id || "";

    Stats.updateStatistics(interaction, "");

    let skip = true;
    const member = await interaction.guild?.members.fetch(user);
    if (member) {
      for (const permission of member.permissions) {
        if (permission === "ADMINISTRATOR") skip = false;
      }
    }

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.WARNING,
      "Remove a role",
      `${userMention(
        user
      )} you do not have the necessary permissions to remove ${roleMention(
        role?.id || ""
      )} to ${userMention(target?.id || "")}`
    );
    embed.setImage("https://c.tenor.com/el1y_HRpyGIAAAAC/no-head-shaking.gif");

    if (!skip) {
      const targetMember = await interaction.guild?.members.fetch(
        target?.id || ""
      );
      if (targetMember && role) {
        try {
          await targetMember.roles.remove(role.id);
          embed.setColor(MessageEmbedColor.SUCCESS);
          embed.setDescription(
            `${userMention(user)} removes the role ${roleMention(
              role?.id || ""
            )} to ${userMention(target?.id || "")}`
          );
          embed.setImage(
            "https://c.tenor.com/BUZOVzZoN-gAAAAC/get-up-out-of-my-bed-anime.gif"
          );
        } catch (err) {
          Logger.log(
            LoggerType.ERROR,
            `Error during the /${interaction.commandName} command: ${err}`
          );
        }
      }
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
