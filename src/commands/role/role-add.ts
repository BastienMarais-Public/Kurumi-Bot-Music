import {
  roleMention,
  SlashCommandBuilder,
  userMention,
} from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Logger, LoggerType } from "../../models/logger/logger";
import { Stats } from "../../models/managers/statistics-manager";
import { Security } from "../../models/managers/security-manager";

/**
 * Allows you to add a role to a person (admin role required)
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("role-add")
    .setDescription(
      "Allows you to add a role to a person (admin role required)"
    )
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("Person who will receive the role")
        .setRequired(true)
    )
    .addRoleOption((option) =>
      option
        .setName("role")
        .setDescription("Role to give to the person")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const target = interaction.options.getUser("target");
    const role = interaction.options.getRole("role");
    const user = interaction.user.id;

    Stats.updateStatistics(interaction, "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.WARNING,
      "Add a role",
      `${userMention(
        user
      )} you do not have the necessary permissions to add ${roleMention(
        role?.id || ""
      )} to ${userMention(target?.id || "")}`
    );
    embed.setImage("https://c.tenor.com/el1y_HRpyGIAAAAC/no-head-shaking.gif");

    if (await Security.hasPermission(interaction, "ADMINISTRATOR")) {
      const targetMember = await interaction.guild?.members.fetch(
        target?.id || ""
      );
      if (targetMember && role) {
        try {
          await targetMember.roles.add(role.id);

          embed.setColor(MessageEmbedColor.SUCCESS);
          embed.setDescription(
            `${userMention(user)} gives the role ${roleMention(
              role?.id || ""
            )} to ${userMention(target?.id || "")}`
          );
          embed.setImage(
            "https://c.tenor.com/msfmevhmlDAAAAAC/anime-chibi.gif"
          );
        } catch (err) {
          Logger.log(
            LoggerType.ERROR,
            `Error during the /${interaction.commandName} command: ${err}`
          );
        }
      }
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
