import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  AkanekoCategory,
  Gifs,
  NekobotCategory,
} from "../../models/managers/gifs-manager";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Security } from "../../models/managers/security-manager";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Displays a random gifs from the selected NSFW category
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("nsfw")
    .setDescription("Displays a random gifs from the selected NSWF category")
    .addStringOption((option) =>
      option
        .setName("category")
        .setDescription("The gif category")
        .setRequired(true)
        .addChoice("Ass", "ass")
        .addChoice("BDSM", "bdsm")
        .addChoice("Blowjob", "blowjob")
        .addChoice("Cum", "cum")
        .addChoice("Doujin", "doujin")
        .addChoice("Feet", "feet")
        .addChoice("Femdom", "femdom")
        .addChoice("Glasses", "glasses")
        .addChoice("Hentai", "hentai")
        .addChoice("Maid", "maid")
        .addChoice("Masturbation", "masturbation")
        .addChoice("Orgy", "orgy")
        .addChoice("Panties", "panties")
        .addChoice("Pussy", "pussy")
        .addChoice("School", "school")
        .addChoice("Succubus", "succubus")
        .addChoice("Tentacles", "tentacles")
        .addChoice("Thighs", "thighs")
        .addChoice("Uniform", "uniform")
        .addChoice("Yuri", "yuri")
        .addChoice("Yaoi", "yaoi")
        .addChoice("Neko", "hneko")
        .addChoice("Kitsune", "hkitsune")
        .addChoice("Ecchi", "ecchi")
    )
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("Your gif's target!")
        .setRequired(false)
    ),

  async execute(interaction: CommandInteraction) {
    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.INFO,
      "",
      ""
    );

    const user = interaction.user;
    let target = interaction.options.getUser("target");
    if (target?.id === user.id) target = null;

    Stats.updateStatistics(interaction, "category");

    let description = "";
    let descriptionWithTarget = "";
    let imageUrl = "";
    let title = "Powered By Akaneko";
    const mentionUser = userMention(user.id);
    let mentionTarget = "";
    if (target) mentionTarget = userMention(target.id);

    if (Security.isNSFW(interaction)) {
      switch (interaction.options.getString("category")) {
        case "ass":
          imageUrl = await Gifs.akaneko(AkanekoCategory.ASS);
          description = `${mentionUser} wants some ass !`;
          descriptionWithTarget = `${mentionUser} wants to show ${mentionTarget}'s ass`;
          break;

        case "bdsm":
          imageUrl = await Gifs.akaneko(AkanekoCategory.BDSM);
          description = `${mentionUser} would like to do BDSM`;
          descriptionWithTarget = `${mentionUser} would like to do BDSM with ${mentionTarget}`;
          break;

        case "blowjob":
          imageUrl = await Gifs.akaneko(AkanekoCategory.BLOWJOB);
          description = `${mentionUser} would like to be sucked...`;
          descriptionWithTarget = `${mentionUser} would like to be sucked off by ${mentionTarget}`;
          break;

        case "cum":
          imageUrl = await Gifs.akaneko(AkanekoCategory.CUM);
          description = `${mentionUser} has full balls`;
          descriptionWithTarget = `${mentionUser} wants to empty his balls on ${mentionTarget}`;
          break;

        case "doujin":
          imageUrl = await Gifs.akaneko(AkanekoCategory.DOUJIN);
          description = `${mentionUser} wants to watch some doujin`;
          descriptionWithTarget = `${mentionUser} wants to watch doujin with ${mentionTarget}`;
          break;

        case "feet":
          imageUrl = await Gifs.akaneko(AkanekoCategory.FEET);
          description = `${mentionUser} wants to see feet...`;
          descriptionWithTarget = `${mentionUser} wants to see the little feet of ${mentionTarget}`;
          break;

        case "femdom":
          imageUrl = await Gifs.akaneko(AkanekoCategory.FEMDOM);
          description = `${mentionUser} wants the woman to dominate`;
          descriptionWithTarget = `${mentionUser} wants the woman to dominate with ${mentionTarget}`;
          break;

        case "glasses":
          imageUrl = await Gifs.akaneko(AkanekoCategory.GLASSES);
          description = `${mentionUser} wants a woman with glasses`;
          descriptionWithTarget = `${mentionUser} shows a woman with glasses at ${mentionTarget}`;
          break;

        case "hentai":
          imageUrl = await Gifs.akaneko(AkanekoCategory.HENTAI);
          description = `${mentionUser} is BAKA HENTAI!!!!`;
          descriptionWithTarget = `${mentionUser} denounces ${mentionTarget} as a BAKA HENTAI!!!!`;
          break;

        case "maid":
          imageUrl = await Gifs.akaneko(AkanekoCategory.MAID);
          description = `${mentionUser} wants a maid`;
          descriptionWithTarget = `${mentionUser} would like ${mentionTarget} to be the maid`;
          break;

        case "masturbation":
          imageUrl = await Gifs.akaneko(AkanekoCategory.MASTURBATION);
          description = `${mentionUser} is masturbating`;
          descriptionWithTarget = `${mentionUser} and ${mentionTarget} masturbate each other`;
          break;

        case "orgy":
          imageUrl = await Gifs.akaneko(AkanekoCategory.ORGY);
          description = `${mentionUser} wants to organize an orgy`;
          descriptionWithTarget = `${mentionUser} and ${mentionTarget} participate in an orgy`;
          break;

        case "panties":
          imageUrl = await Gifs.akaneko(AkanekoCategory.PANTIES);
          description = `${mentionUser} wants to see underwear....`;
          descriptionWithTarget = `${mentionUser} wants to see ${mentionTarget}'s little clothes...`;
          break;

        case "pussy":
          imageUrl = await Gifs.akaneko(AkanekoCategory.PUSSY);
          description = `${mentionUser} is curious...`;
          descriptionWithTarget = `${mentionUser} and ${mentionTarget} are looking at some pussy`;
          break;

        case "school":
          imageUrl = await Gifs.akaneko(AkanekoCategory.SCHOOL);
          description = `${mentionUser} finally wants to go back to school`;
          descriptionWithTarget = `${mentionUser} and ${mentionTarget} enjoy their studies`;
          break;

        case "succubus":
          imageUrl = await Gifs.akaneko(AkanekoCategory.SUCCUBUS);
          description = `${mentionUser} is interested in the occult all of a sudden`;
          descriptionWithTarget = `${mentionUser} discovers the evil side of ${mentionTarget}`;
          break;

        case "tentacles":
          imageUrl = await Gifs.akaneko(AkanekoCategory.TENTACLES);
          description = `${mentionUser} has always had a crush on the Kraken...`;
          descriptionWithTarget = `${mentionUser} triggers a tentacular attack on ${mentionTarget}`;
          break;

        case "thighs":
          imageUrl = await Gifs.akaneko(AkanekoCategory.THIGHS);
          description = `${mentionUser} wants to see some thighs`;
          descriptionWithTarget = `${mentionUser} wants to see the thighs of ${mentionTarget}`;
          break;

        case "uniform":
          imageUrl = await Gifs.akaneko(AkanekoCategory.UNIFORM);
          description = `${mentionUser} finds uniforms sexy`;
          descriptionWithTarget = `${mentionUser} wants to see ${mentionTarget} in uniform`;
          break;

        case "yuri":
          imageUrl = await Gifs.akaneko(AkanekoCategory.YURI);
          description = `${mentionUser} thinks that one woman is good but several is even better!`;
          descriptionWithTarget = `${mentionUser} invites ${mentionTarget} to a party`;
          break;

        case "yaoi":
          title = "Powered By nekobot.xyz";
          imageUrl = await Gifs.nekobot(NekobotCategory.YAOI);
          description = `${mentionUser} thinks that one man is good but several is even better!`;
          descriptionWithTarget = `${mentionUser} invites ${mentionTarget} to a party`;
          break;

        case "hneko":
          title = "Powered By nekobot.xyz";
          imageUrl = await Gifs.nekobot(NekobotCategory.HNEKO);
          description = `${mentionUser} wants to adopt a "cat"`;
          descriptionWithTarget = `${mentionUser} adopts ${mentionTarget} as new "cat"`;
          break;

        case "hkitsune":
          title = "Powered By nekobot.xyz";
          imageUrl = await Gifs.nekobot(NekobotCategory.HKITSUNE);
          description = `${mentionUser} loves foxes`;
          descriptionWithTarget = `${mentionUser} finds ${mentionTarget} smart like a fox...`;
          break;

        case "ecchi":
          title = "Powered By nekos.life";
          imageUrl = await Gifs.ecchi();
          description = `${mentionUser} begins "soft"`;
          descriptionWithTarget = `The temperature rises between ${mentionUser} and ${mentionTarget}`;
          break;

        default:
          title = "Powered By I don't know...";
          imageUrl = "https://i.gifer.com/5GpB.gif";
          description = "I don't know what to do!!!!!!";
          descriptionWithTarget = `Sorry ${mentionUser} I don't know what to do with ${mentionTarget}!!!!!!`;
          break;
      }
    } else {
      embed.setColor(MessageEmbedColor.WARNING);
      title = "Warning";
      imageUrl = "https://c.tenor.com/CjOO1miF0VAAAAAd/pervert-anime.gif";
      target = null;
      description = `${mentionUser} this is not an NSFW channel here...`;
    }

    if (target) {
      embed.setDescription(descriptionWithTarget);
    } else {
      embed.setDescription(description);
    }

    embed.setImage(imageUrl);
    embed.setTitle(title);

    await interaction.editReply({ embeds: [embed] });
  },
};
