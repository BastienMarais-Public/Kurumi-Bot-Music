import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";
import { Counter } from "../../models/managers/counters-manager";

/**
 * Increases the counter of a person by 1
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("counter-add")
    .setDescription("Increases the counter of a person by 1")
    .addStringOption((option) =>
      option
        .setName("counter")
        .setDescription("The name of the counter")
        .setRequired(true)
        .addChoice("Ban", "ban")
        .addChoice("Death", "death")
    )
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("The target of this counter")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const target = interaction.options.getUser("target")?.id;
    const guildId = interaction.guildId;
    const name = interaction.options.getString("counter");
    const user = interaction.user.id;

    Stats.updateStatistics(interaction, name || "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.ERROR,
      "Increasing a counter",
      "This command can only be used on a server"
    );

    if (target && user && name && guildId) {
      await Counter.addValueToCounter(guildId, name, target, 1);

      embed.setColor(MessageEmbedColor.SUCCESS);
      embed.setTitle("Increasing a counter");
      embed.setDescription(
        `Updating the ${name} counter on ${userMention(target)}`
      );

      switch (name) {
        case "ban":
          embed.setImage("https://c.tenor.com/SipkXgk-Kd8AAAAC/anime-mad.gif");
          embed.setDescription(
            `${userMention(user)} threatens to ban ${userMention(target)}`
          );
          break;

        case "death":
          embed.setImage(
            "https://c.tenor.com/bTeYzmeM4YQAAAAC/youre-dead-angels-of-death.gif"
          );
          embed.setDescription(
            `${userMention(user)} threatens to kill ${userMention(target)}`
          );
          break;
      }
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
