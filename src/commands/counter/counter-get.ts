import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";
import { Counter } from "../../models/managers/counters-manager";
import { capitalizeFirstLetter } from "../../models/utils";

/**
 * Displays the counters of a person
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("counter-get")
    .setDescription("Displays the counters of a person")
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("The target of this counter")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const target = interaction.options.getUser("target")?.id;
    const guildId = interaction.guildId;

    Stats.updateStatistics(interaction, "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.ERROR,
      `Counter information`,
      "This command can only be used on a server"
    );

    if (target && guildId) {
      const data = await Counter.getAllCounters(guildId, target);
      embed.setColor(MessageEmbedColor.SUCCESS);
      embed.setDescription(`List of ${userMention(target)} counters`);

      for (const row of data) {
        embed.addField(
          `${capitalizeFirstLetter(row.name)}`,
          `${row.value}`,
          true
        );
      }
      if (embed.fields.length === 0) {
        embed.setColor(MessageEmbedColor.WARNING);
        embed.setDescription(
          `There is nothing to display for ${userMention(target)}...`
        );
      }
    }
    await interaction.editReply({ embeds: [embed] });
  },
};
