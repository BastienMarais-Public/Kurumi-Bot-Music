import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";
import { Counter } from "../../models/managers/counters-manager";

/**
 * Decreases the counter of a person by 1
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("counter-minus")
    .setDescription("Decreases the counter of a person by 1")
    .addStringOption((option) =>
      option
        .setName("counter")
        .setDescription("The name of the counter")
        .setRequired(true)
        .addChoice("Ban", "ban")
        .addChoice("Death", "death")
    )
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("The target of this counter")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const target = interaction.options.getUser("target")?.id;
    const guildId = interaction.guildId;
    const name = interaction.options.getString("counter");
    const user = interaction.user.id;

    Stats.updateStatistics(interaction, name || "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.ERROR,
      "Decreasing a counter",
      "This command can only be used on a server"
    );

    if (target && guildId && name && user) {
      await Counter.addValueToCounter(guildId, name, target, -1);

      embed.setColor(MessageEmbedColor.SUCCESS);
      embed.setDescription(
        `Updating the ${name} counter on ${userMention(target)}`
      );

      switch (name) {
        case "ban":
          embed.setImage(
            "https://c.tenor.com/e7nZinqmOA4AAAAC/objection-pheonix-wright.gif"
          );
          embed.setDescription(
            `${userMention(user)} cancels ban threat ${userMention(target)}`
          );
          break;

        case "death":
          embed.setImage(
            "https://c.tenor.com/6lYhzDR9HewAAAAC/dr-stone-smiling.gif"
          );
          embed.setDescription(
            `${userMention(user)} cancels death threat ${userMention(target)}`
          );
          break;
      }
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
