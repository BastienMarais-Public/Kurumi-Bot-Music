import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";
import { Counter } from "../../models/managers/counters-manager";

/**
 * Display the top of a counter on the server
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("counter-top")
    .setDescription("Display the top of a counter on the server")
    .addStringOption((option) =>
      option
        .setName("counter")
        .setDescription("The name of the counter")
        .setRequired(true)
        .addChoice("Ban", "ban")
        .addChoice("Death", "death")
    ),

  async execute(interaction: CommandInteraction) {
    const guildId = interaction.guildId;
    const name = interaction.options.getString("counter");

    Stats.updateStatistics(interaction, name || "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.ERROR,
      `Top counter`,
      "This command can only be used on a server"
    );

    if (guildId && name) {
      const limit = 10;
      embed.setColor(MessageEmbedColor.INFO);
      embed.setDescription(`Top ${limit} of the ${name} counter on the server`);

      const data = await Counter.getTopCounters(guildId, name, limit);
      let i = 1;
      for (const row of data) {
        embed.addField(
          `Number #${i}`,
          `${userMention(row.target)} with ${row.value}`
        );
        i += 1;
      }
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
