import {
  hyperlink,
  SlashCommandBuilder,
  userMention,
} from "@discordjs/builders";
import { CommandInteraction, MessageEmbed } from "discord.js";
import { AnimeSubscribes } from "../../models/database/tables/anime-subscribes";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Allows to list the anime on which the user is subscribed
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("anime-list")
    .setDescription("See which anime you are registered for"),

  async execute(interaction: CommandInteraction) {
    Stats.updateStatistics(interaction, "");

    const subscribes = await AnimeSubscribes.findAll({
      where: {
        discordUserId: interaction.user.id,
      },
    });

    const listEmbed: MessageEmbed[] = [];
    let counter = 0;
    let embed: MessageEmbed = new MessageEmbed();
    const title = "Anime-List";
    for (const subscribe of subscribes) {
      if (counter === 0) {
        embed = EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.INFO,
          title,
          `${userMention(
            interaction.user.id
          )} here is the list of anime you are subscribed to:`
        );
      }
      embed.addField(
        subscribe.animeName,
        hyperlink("Anime link", subscribe.animeUrl)
      );

      if (counter === 24) {
        listEmbed.push(embed);
        counter = -1;
      }
      counter += 1;
    }
    if (embed.fields.length) {
      listEmbed.push(embed);
    }
    if (listEmbed.length === 0)
      listEmbed.push(
        EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.WARNING,
          title,
          "There is nothing to display..."
        )
      );

    await interaction.editReply({ embeds: listEmbed });
  },
};
