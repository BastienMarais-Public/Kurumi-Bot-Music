import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { AnimeSubscribes } from "../../models/database/tables/anime-subscribes";
import fetch from "node-fetch";
import { Stats } from "../../models/managers/statistics-manager";
import * as config from "../../config.json";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";

/**
 * Allows you to unsubscribe from an anime
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("anime-unsubscribe")
    .setDescription("Unsubscribe to an anime")
    .addStringOption((option) =>
      option
        .setName("anime_url")
        .setDescription(`The URL on ${config.animeSiteName} of the anime!`)
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const animeURL: string = interaction.options.getString("anime_url") || "";

    Stats.updateStatistics(interaction, "");

    const matcherLink = new RegExp(
      `https:\\/\\/(www\\.)?${config.animeDomainName.replace(
        ".",
        "\\."
      )}\\/anime\\/info\\/.+`
    );
    const isValidLink = matcherLink.test(animeURL!);

    if (!isValidLink) {
      return interaction.editReply({
        embeds: [
          EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.ERROR,
            "Anime-Unsubscribe",
            `❌ You need to provide a correct ${config.animeSiteName} URL!`
          ),
        ],
      });
    }

    try {
      await fetch(animeURL);
    } catch (err) {
      return interaction.editReply({
        embeds: [
          EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.ERROR,
            "Anime-Unsubscribe",
            `❌ You need to provide a working ${config.animeSiteName} URL!`
          ),
        ],
      });
    }

    const rowCount = await AnimeSubscribes.destroy({
      where: { animeUrl: animeURL, discordUserId: interaction.user.id },
    });

    if (!rowCount) {
      return interaction.editReply({
        embeds: [
          EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.ERROR,
            "Anime-Unsubscribe",
            `❌ You are not subscribed to ${animeURL} !`
          ),
        ],
      });
    }
    return interaction.editReply({
      embeds: [
        EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.SUCCESS,
          "Anime-Unsubscribe",
          `✅ You have been unsubscribed from ${animeURL} !`
        ),
      ],
    });
  },
};
