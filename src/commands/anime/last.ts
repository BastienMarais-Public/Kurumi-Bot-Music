import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { capitalizeFirstLetter } from "../../models/utils";
import * as config from "../../config.json";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import fetch from "node-fetch";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Shows the latest episodes released on https://${config.animeDomainName}/
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("anime-last")
    .setDescription(
      `Shows the latest episodes released on https://${config.animeDomainName}/`
    ),

  async execute(interaction: CommandInteraction) {
    Stats.updateStatistics(interaction, "");

    const response = await fetch(
      "https://nekosama.codexus.fr/api/episodes/today"
    );
    let embed = null;

    if (response.status === 200) {
      const json = await response.json();
      let old = { title: "" };
      let same = [];
      let skip = false;
      for (const episode of json) {
        if (!embed) {
          embed = EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.INFO,
            `Here are the latest episodes released on ${config.animeSiteName}`,
            ""
          );
        }
        if (old.title !== episode.title) {
          if (skip) {
            skip = false;
            embed.addField(
              `${capitalizeFirstLetter(same[0].time)}`,
              `[[${same[0].episode}] ${same[0].title}](https://${config.animeDomainName}${same[0].url})`,
              true
            );
            embed.addField(
              `${capitalizeFirstLetter(same[same.length - 1].time)}`,
              `[[${same[same.length - 1].episode}] ${
                same[same.length - 1].title
              }](https://${config.animeDomainName}${
                same[same.length - 1].url
              })`,
              true
            );
            same = [];
          }
          embed.addField(
            `${capitalizeFirstLetter(episode.time)}`,
            `[[${episode.episode}] ${episode.title}](https://${config.animeDomainName}${episode.url})`
          );
          old = episode;
        } else {
          skip = true;
          same.push(episode);
        }
      }
    } else {
      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.ERROR,
        `Here are the latest episodes released on ${config.animeSiteName}`,
        ""
      );
      embed.addFields({
        name: "Result",
        value: "Error when requesting api...",
      });
    }

    if (!embed) {
      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.WARNING,
        `Here are the latest episodes released on ${config.animeSiteName}`,
        ""
      );
      embed.addFields({ name: "Result", value: "No result..." });
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
