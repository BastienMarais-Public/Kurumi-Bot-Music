import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import * as config from "../../config.json";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import fetch from "node-fetch";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Allows you to search for an anime on https://${config.animeDomainName}/
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("anime-search")
    .setDescription(
      `Allows you to search for an anime on https://${config.animeDomainName}/`
    )
    .addStringOption((option) =>
      option
        .setName("anime_name")
        .setDescription("The name of the anime you are looking for")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const animeName = interaction.options.getString("anime_name") || "";

    Stats.updateStatistics(interaction, "");

    const limit = 10;
    const response = await fetch(
      `https://nekosama.codexus.fr/api/animes?search=${animeName.toLowerCase()}&perPage=${limit}&sort=start_date_year_asc`
    );
    let embed = null;

    if (response.status === 200) {
      const json = await response.json();
      let counter = 0;
      for (const anime of json.animes) {
        counter += 1;
        if (!embed) {
          embed = EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.INFO,
            "Here are the anime that could match",
            ""
          );
        }
        embed.addFields({
          name: `Result #${counter}`,
          value: `[${anime.title} ](https://${config.animeDomainName}${anime.url})`,
        });
      }
    } else {
      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.ERROR,
        "Here are the anime that could match",
        ""
      );
      embed.addFields({
        name: "Result",
        value: "Error when requesting api...",
      });
    }

    if (!embed) {
      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.ERROR,
        "Here are the anime that could match",
        ""
      );
      embed.addFields({ name: "Result", value: "No result..." });
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
