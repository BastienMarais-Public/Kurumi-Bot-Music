import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { AnimeSubscribes } from "../../models/database/tables/anime-subscribes";
import fetch from "node-fetch";
import { Stats } from "../../models/managers/statistics-manager";
import * as config from "../../config.json";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";

/**
 * Allows you to subscribe to an anime and receive episode release notifications
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("anime-subscribe")
    .setDescription(
      "Subscribe to an anime to be notified when a new episode is released!"
    )
    .addStringOption((option) =>
      option
        .setName("anime_url")
        .setDescription(`The URL on ${config.animeDomainName} of the anime!`)
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const animeURL = interaction.options.getString("anime_url") || "";

    Stats.updateStatistics(interaction, "");

    const matcherLink = new RegExp(
      `https:\\/\\/(www\\.)?${config.animeDomainName.replace(
        ".",
        "\\."
      )}\\/anime\\/info\\/.+`
    );
    const isValidLink = matcherLink.test(animeURL!);

    if (!isValidLink) {
      return interaction.editReply({
        embeds: [
          EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.ERROR,
            "Anime-Subscribe",
            `❌ You need to provide a correct ${config.animeSiteName} URL!`
          ),
        ],
      });
    }

    let response;
    try {
      response = await fetch(animeURL);
    } catch (err) {
      return interaction.editReply({
        embeds: [
          EmbedFactory.generateMessageEmbed(
            MessageEmbedColor.ERROR,
            "Anime-Subscribe",
            `❌ You need to provide a working ${config.animeSiteName} URL!`
          ),
        ],
      });
    }

    const html: string = await response.text();
    const matcherTitle = new RegExp(
      `<title>(.+)<\\/title>`
    );
    let anime_name: string = html.match(matcherTitle)![1]!;
    anime_name = anime_name.split(" VOSTFR")[0]!;
    anime_name = anime_name.split(" VF")[0]!;

    const episodes = JSON.parse(html.match(/var episodes = (\[.+]);/)![1]!);

    let latestEpisodeNumber: number = 0;
    for (const episode of episodes) {
      const episode_number = parseInt(
        (episode.episode as string).replace("Ep. ", ""),
        10
      );

      if (episode_number > latestEpisodeNumber)
        latestEpisodeNumber = episode_number;
    }

    await AnimeSubscribes.create({
      animeUrl: animeURL,
      animeName: anime_name,
      discordUserId: interaction.user.id,
      latestEpisode: latestEpisodeNumber,
    });

    return interaction.editReply({
      embeds: [
        EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.SUCCESS,
          "Anime-Subscribe",
          `✅ ${anime_name} has been registered ! You will receive a message in MP for each new episode!`
        ),
      ],
    });
  },
};
