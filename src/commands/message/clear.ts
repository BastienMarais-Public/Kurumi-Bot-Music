import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Logger, LoggerType } from "../../models/logger/logger";
import { Stats } from "../../models/managers/statistics-manager";
import { Security } from "../../models/managers/security-manager";

/**
 * Allows you to delete the last 'x' messages, 20 by default
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("clear")
    .setDescription("Allows you to delete the last 'x' messages, 20 by default")
    .addNumberOption((option) =>
      option
        .setName("x")
        .setDescription("Number of messages to delete")
        .setRequired(false)
    ),

  async execute(interaction: CommandInteraction) {
    let nbMsg = interaction.options.getNumber("x") || 20;
    if (nbMsg < 0) nbMsg = 1;

    Stats.updateStatistics(interaction, "");

    let embed;

    if (
      (await Security.hasPermission(interaction, "MANAGE_MESSAGES")) ||
      !interaction.inGuild()
    ) {
      let messages;
      let myMsg = "";
      let counter = 0;
      let filter: any = { limit: 100 };

      for (let i = 0; i < (nbMsg + 1) / 100; i++) {
        if (myMsg !== "") filter = { limit: 100, before: myMsg };
        messages = await interaction.channel?.messages.fetch(filter);
        if (messages) {
          for (const msg of (messages as any).keys()) {
            if (counter !== 0) {
              if (counter > nbMsg) break;
              myMsg = msg;
              setTimeout(() => {
                try {
                  interaction.channel?.messages.delete(msg);
                } catch (err) {
                  Logger.log(
                    LoggerType.WARN,
                    `Error during message deletion: ${err}`
                  );
                }
              }, 1000);
            }
            counter += 1;
          }
        }
      }

      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.SUCCESS,
        "Channel cleaning",
        `${userMention(interaction.user.id)} I have requested the deletion of ${
          counter - 1
        } message${counter > 2 ? "s" : ""}`
      );
      embed.setImage(
        "https://c.tenor.com/0j2Kf4vZ4GMAAAAd/nekopara-cleaning.gif"
      );
    } else {
      embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.WARNING,
        "Channel cleaning",
        `You do not have the necessary permissions to clean a channel`
      );
      embed.setImage(
        "https://c.tenor.com/el1y_HRpyGIAAAAC/no-head-shaking.gif"
      );
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
