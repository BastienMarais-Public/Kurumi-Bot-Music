import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Logger, LoggerType } from "../../models/logger/logger";
import { Stats } from "../../models/managers/statistics-manager";
import { Security } from "../../models/managers/security-manager";

/**
 * Allows to broadcast an announcement to the owners and admins of the servers
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("send-all")
    .setDescription(
      "Allows to broadcast an announcement to the owners and admins of the servers"
    )
    .addStringOption((option) =>
      option
        .setName("message")
        .setDescription("The announcement to send")
        .setRequired(true)
    ),

  async execute(interaction: CommandInteraction) {
    const msg = interaction.options.getString("message");

    Stats.updateStatistics(interaction, "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.ERROR,
      "Broadcasting of the announcement",
      "My creator is very selfish, he reserves this command for himself :yum:"
    );

    if (Security.isBotOwner(interaction) && msg) {
      const announcement = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.WARNING,
        ":bell: Important announcement :bell:",
        msg
      );
      const targets: string[] = [];
      for (const guildId of (await interaction.client.guilds.fetch()).keys()) {
        const guild = await interaction.client.guilds.resolve(guildId);
        if (guild) {
          const users = (await guild.members.fetch()).filter((member) => {
            return (
              (member.permissions.has("ADMINISTRATOR") ||
                member.id === guild.ownerId) &&
              !member.user.bot &&
              !targets.includes(member.id)
            );
          });
          for (const user of users) {
            try {
              await user[1].user.send({ embeds: [announcement] });
              targets.push(user[0]);
              Logger.log(
                LoggerType.INFO,
                `Send message to ${Logger.getUserStr(user[1].user)}`
              );
            } catch (err) {
              Logger.log(
                LoggerType.WARN,
                `Cannot send message to ${Logger.getUserStr(user[1].user)}`
              );
            }
          }
        }
      }
      embed.setColor(MessageEmbedColor.SUCCESS);
      embed.setDescription(
        `${userMention(interaction.user.id)} I have sent the announcement`
      );
      embed.addField("Content", msg);
      embed.addField("Number of messages sent", `${targets.length} :envelope:`);
    } else {
      embed.setImage(
        "https://c.tenor.com/xSv3bP-S4SkAAAAd/anime-nope-nope.gif"
      );
    }

    await interaction.editReply({ embeds: [embed] });
  },
};
