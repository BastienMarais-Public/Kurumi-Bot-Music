import { SlashCommandBuilder, userMention } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { Gifs } from "../../models/managers/gifs-manager";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Displays a random gifs from the selected SFW category
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("gif")
    .setDescription("Displays a random gifs from the selected SFW category")
    .addStringOption((option) =>
      option
        .setName("category")
        .setDescription("The gif category")
        .setRequired(true)
        .addChoice("Angry", "angry")
        .addChoice("Bite", "bite")
        .addChoice("Blush", "blush")
        .addChoice("Coffee", "coffee")
        .addChoice("Cuddle", "cuddle")
        .addChoice("Dance", "dance")
        .addChoice("Fight", "fight")
        .addChoice("Hug", "hug")
        .addChoice("Hype", "hype")
        .addChoice("Kick", "kick")
        .addChoice("Kiss", "kiss")
        .addChoice("Kurumi", "kurumi")
        .addChoice("Neko", "neko")
        .addChoice("Panda", "panda")
        .addChoice("Pat", "pat")
        .addChoice("Poke", "poke")
        .addChoice("Pokemon", "pokemon")
        .addChoice("Sad", "sad")
        .addChoice("Slap", "slap")
        .addChoice("Tickle", "tickle")
        .addChoice("Waifu", "waifu")
    )
    .addUserOption((option) =>
      option
        .setName("target")
        .setDescription("Your gif's target!")
        .setRequired(false)
    ),

  async execute(interaction: CommandInteraction) {
    const user = interaction.user;
    let target = interaction.options.getUser("target");
    if (target && target.id === user.id) target = null;

    Stats.updateStatistics(interaction, "category");

    let description: string;
    let descriptionWithTarget: string;
    let imageUrl: string;
    let title = "Custom Gifs Manager";
    const mentionUser = userMention(user.id);
    let mentionTarget = "";
    if (target) mentionTarget = userMention(target.id);

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.INFO,
      title,
      ""
    );

    switch (interaction.options.getString("category")) {
      case "angry":
        imageUrl = await Gifs.customGifsManager("Angry");
        description = `${mentionUser} is angry...`;
        descriptionWithTarget = `${mentionUser} is angry against ${mentionTarget}`;
        break;

      case "bite":
        imageUrl = await Gifs.customGifsManager("Bite");
        description = `${mentionUser} wants to bite someone...`;
        descriptionWithTarget = `${mentionUser} bites ${mentionTarget}`;
        break;

      case "blush":
        imageUrl = await Gifs.customGifsManager("Blush");
        description = `${mentionUser} turns red...`;
        descriptionWithTarget = `${mentionUser} blushed in front of ${mentionTarget}`;
        break;

      case "coffee":
        title = "Powered By nekobot.xyz";
        imageUrl = await Gifs.customGifsManager("Coffee");
        description = `${mentionUser} needs a coffee...`;
        descriptionWithTarget = `${mentionUser} offers a coffee to ${mentionTarget}`;
        break;

      case "cuddle":
        imageUrl = await Gifs.customGifsManager("Cuddle");
        description = `${mentionUser} wants to be cuddled...`;
        descriptionWithTarget = `${mentionUser} cuddles ${mentionTarget}`;
        break;

      case "dance":
        imageUrl = await Gifs.customGifsManager("Dance");
        description = `${mentionUser} wants to dance...`;
        descriptionWithTarget = `${mentionUser} dance with ${mentionTarget}`;
        break;

      case "fight":
        imageUrl = await Gifs.customGifsManager("Fight");
        description = `${mentionUser} wants to fight...`;
        descriptionWithTarget = `${mentionUser} fights with ${mentionTarget}`;
        break;

      case "hug":
        imageUrl = await Gifs.customGifsManager("Hug");
        description = `${mentionUser} wants a hug...`;
        descriptionWithTarget = `${mentionUser} hugs ${mentionTarget}`;
        break;

      case "hype":
        imageUrl = await Gifs.customGifsManager("Hype");
        description = `${mentionUser} is totally hyped`;
        descriptionWithTarget = `${mentionUser} and ${mentionTarget} are totally hyped`;
        break;

      case "kick":
        imageUrl = await Gifs.customGifsManager("Kick");
        description = `${mentionUser} wants to kick someone...`;
        descriptionWithTarget = `${mentionUser} kicks ${mentionTarget}`;
        break;

      case "kiss":
        imageUrl = await Gifs.customGifsManager("Kiss");
        description = `${mentionUser} wants a kiss...`;
        descriptionWithTarget = `${mentionUser} gives a kiss to ${mentionTarget}`;
        break;

      case "kurumi":
        imageUrl = await Gifs.customGifsManager("Kurumi");
        description = `${mentionUser} summons Kurumi`;
        descriptionWithTarget = `${mentionUser} summons Kurumi for ${mentionTarget}`;
        break;

      case "neko":
        imageUrl = await Gifs.customGifsManager("Neko");
        description = `${mentionUser} needs a cat...`;
        descriptionWithTarget = `${mentionUser} gives a cat to ${mentionTarget}`;
        break;

      case "panda":
        imageUrl = await Gifs.customGifsManager("Panda");
        description = `${mentionUser} needs a panda...`;
        descriptionWithTarget = `${mentionUser} gives a panda to ${mentionTarget}`;
        break;

      case "pat":
        imageUrl = await Gifs.customGifsManager("Pat");
        description = `${mentionUser} needs a pat...`;
        descriptionWithTarget = `${mentionUser} gives a pat to ${mentionTarget}`;
        break;

      case "poke":
        imageUrl = await Gifs.customGifsManager("Poke");
        description = `${mentionUser} wants to piss someone off...`;
        descriptionWithTarget = `${mentionUser} pokes ${mentionTarget}`;
        break;

      case "pokemon":
        imageUrl = await Gifs.customGifsManager("Pokemon");
        description = `${mentionUser} is a pokemon...`;
        descriptionWithTarget = `${mentionUser} wants to make a pokemon exchange with ${mentionTarget}`;
        break;

      case "sad":
        imageUrl = await Gifs.customGifsManager("Sad");
        description = `${mentionUser} is sad...`;
        descriptionWithTarget = `${mentionUser} cries oncries on ${mentionTarget}'s shoulder`;
        break;

      case "slap":
        imageUrl = await Gifs.customGifsManager("Slap");
        description = `${mentionUser} wants to give away slaps...`;
        descriptionWithTarget = `${mentionUser} slaps ${mentionTarget} in the face`;
        break;

      case "tickle":
        imageUrl = await Gifs.customGifsManager("Tickle");
        description = `${mentionUser} wants to tickle someone...`;
        descriptionWithTarget = `${mentionUser} tickles ${mentionTarget}`;
        break;

      case "waifu":
        imageUrl = await Gifs.customGifsManager("Waifu");
        description = `${mentionUser} needs a waifu...`;
        descriptionWithTarget = `${mentionUser} offers a waifu to ${mentionTarget}`;
        break;

      default:
        imageUrl = "https://i.gifer.com/5GpB.gif";
        description = "I don't know what to do!!!!!!";
        descriptionWithTarget = `Sorry ${mentionUser} I don't know what to do with ${mentionTarget}!!!!!!`;
        break;
    }

    if (target) {
      embed.setDescription(descriptionWithTarget);
    } else {
      embed.setDescription(description);
    }

    embed.setImage(imageUrl);
    embed.setTitle(title);

    await interaction.editReply({ embeds: [embed] });
  },
};
