import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, MessageEmbed } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import { Stats } from "../../models/managers/statistics-manager";
import { getCommands } from "../../models/managers/commands-manager";
import * as config from "../../config.json";

/**
 * Displays the list of commands
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("help")
    .setDescription("Displays the list of commands"),

  async execute(interaction: CommandInteraction) {
    Stats.updateStatistics(interaction, "");

    const listEmbed: MessageEmbed[] = [];
    let embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.INFO,
      "List of commands",
      ""
    );
    const commands = await (
      await getCommands(
        `${config.isProd ? "dist" : "src"}/commands`,
        "../../commands"
      )
    ).json;
    let counter = 1;
    for (const command of commands) {
      if (counter === 0) {
        embed = EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.INFO,
          "List of commands 2/2",
          ""
        );
      }
      embed.addField(command.name, command.description, true);
      if (counter === 24) {
        embed.setTitle("List of commands 1/2");
        listEmbed.push(embed);
        counter = -1;
      }
      counter += 1;
    }

    if (counter !== 0) {
      listEmbed.push(embed);
    }

    await interaction.editReply({ embeds: listEmbed });
  },
};
