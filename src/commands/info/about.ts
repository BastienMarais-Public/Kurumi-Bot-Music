import { hyperlink, SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import * as config from "../../config.json";
import { Stats } from "../../models/managers/statistics-manager";

/**
 * Displays the bot's information
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("about")
    .setDescription("Displays the bot's information"),

  async execute(interaction: CommandInteraction) {
    Stats.updateStatistics(interaction, "");

    const embed = EmbedFactory.generateMessageEmbed(
      MessageEmbedColor.INFO,
      "Information about the bot",
      `This bot allows various commands for gifs (SFW and NSFW), admin and utility functions for ${config.animeDomainName}.`
    );
    embed.addField(
      "Developper",
      hyperlink("Bastien Marais", "https://bastienmarais.fr/")
    );
    embed.addField(
      "Project",
      hyperlink(
        "On Gitlab",
        "https://gitlab.com/BastienMarais-Public/Kurumi-Bot/"
      )
    );
    embed.addField(
      "Credits",
      `Thanks to ${hyperlink(
        "Astropilot",
        "https://github.com/Astropilot/"
      )} for allowing me to use its APIs for ${
        config.animeDomainName
      } and anime-subscribe code. Thanks also to the discord server of the SAV which supports all my tests :P !`
    );
    embed.addField(
      "Share bot",
      hyperlink(
        "Invitation link of the bot",
        `https://discord.com/api/oauth2/authorize?client_id=${config.clientId}&permissions=8&scope=bot%20applications.commands`
      )
    );

    await interaction.editReply({ embeds: [embed] });
  },
};
