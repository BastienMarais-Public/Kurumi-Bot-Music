import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, MessageEmbed } from "discord.js";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../../models/managers/messageEmbed-factory";
import {
  StatisticsData,
  Stats,
} from "../../models/managers/statistics-manager";

/**
 * Displays the statistics of the bot
 */
module.exports = {
  data: new SlashCommandBuilder()
    .setName("stats")
    .setDescription("Displays the statistics of the bot")
    .addStringOption((option) =>
      option
        .setName("scope")
        .setDescription("The scope of the statistics ")
        .setRequired(true)
        .addChoice("This server", "local")
        .addChoice("Global", "global")
    ),

  async execute(interaction: CommandInteraction) {
    const scope = interaction.options.getString("scope");

    Stats.updateStatistics(interaction, "scope");

    let title = "";
    let data: StatisticsData[] = [];
    if (scope === "local") {
      const guildId = interaction.guildId;
      title = "Local statistics of the bot";
      if (guildId) {
        data = await Stats.getLocalTopStatistics(guildId);
      }
    } else {
      title = "Global statistics of the bot";
      data = await Stats.getGlobalTopStatistics();
    }

    const listEmbed: MessageEmbed[] = [];
    let counter = 0;
    let embed: MessageEmbed = new MessageEmbed();
    for (const row of data) {
      if (counter === 0) {
        embed = EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.INFO,
          title,
          ""
        );
      }
      if (row.option === "") {
        embed.addField(`${row.command}`, row.use.toString(), true);
      } else {
        embed.addField(
          `${row.command} > ${row.option}`,
          row.use.toString(),
          true
        );
      }
      if (counter === 24) {
        listEmbed.push(embed);
        counter = -1;
      }
      counter += 1;
    }
    if (embed.fields.length) {
      listEmbed.push(embed);
    }
    if (listEmbed.length === 0)
      listEmbed.push(
        EmbedFactory.generateMessageEmbed(
          MessageEmbedColor.WARNING,
          title,
          "There is nothing to display..."
        )
      );

    await interaction.editReply({ embeds: listEmbed });
  },
};
