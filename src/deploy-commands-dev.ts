import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import * as config from "./config.json";
import { Logger, LoggerType } from "./models/logger/logger";
import { getCommands } from "./models/managers/commands-manager";

/**
 * Allows to register all bot commands on the dev server
 */
(async () => {
  const commands = await getCommands("./src/commands", "../../commands");
  const rest = new REST({ version: "9" }).setToken(config.token);

  for (const guildId of config.guildId) {
    if (guildId !== "") {
      rest
        .put(Routes.applicationGuildCommands(config.clientId, guildId), {
          body: commands.json,
        })
        .then(() =>
          Logger.log(
            LoggerType.INFO,
            `[${guildId}] Successfully registered application guild commands.`
          )
        )
        .catch(console.error);
    }
  }
})();
