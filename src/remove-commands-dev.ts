import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import * as config from "./config.json";
import { Logger, LoggerType } from "./models/logger/logger";

/**
 * Allows to remove all bot commands on the dev server
 */
(() => {
  const rest = new REST({ version: "9" }).setToken(config.token);

  for (const guildId of config.guildId) {
    rest
      .get(Routes.applicationGuildCommands(config.clientId, guildId))
      .then((data: any) => {
        const promises = [];
        for (const command of data) {
          const deleteUrl: any = `${Routes.applicationGuildCommands(
            config.clientId,
            guildId
          )}/${command.id}`;
          Logger.log(
            LoggerType.INFO,
            `[${guildId}] Deleting the command: ${command.name}`
          );
          promises.push(rest.delete(deleteUrl));
        }
        return Promise.all(promises);
      });
  }
})();
