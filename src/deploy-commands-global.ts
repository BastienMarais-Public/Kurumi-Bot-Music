import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import * as config from "./config.json";
import { Logger, LoggerType } from "./models/logger/logger";
import { getCommands } from "./models/managers/commands-manager";

/**
 * Allows you to register all the bot's commands on the prod
 */
(async () => {
  const commands = await getCommands("./dist/commands", "../../commands");
  const rest = new REST({ version: "9" }).setToken(config.token);

  rest
    .put(Routes.applicationCommands(config.clientId), { body: commands.json })
    .then(() =>
      Logger.log(
        LoggerType.INFO,
        "Successfully registered application commands."
      )
    )
    .catch(console.error);
})();
