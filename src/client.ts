import { Client, Collection, Intents } from "discord.js";
import * as config from "./config.json";
import { sequelize } from "./models/database/database";
import { Logger, LoggerType } from "./models/logger/logger";
import { getCommands } from "./models/managers/commands-manager";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "./models/managers/messageEmbed-factory";
import { checkSubscribers } from "./models/services/check-subscribers";
import { checkLogs } from "./models/services/check-logs";
import { capitalizeFirstLetter } from "./models/utils";

// Process nodejs
const process = require("process");

/**
 * Procedure for stopping the program
 * @param client Discord client launched
 */
async function cleanStop(client: Client): Promise<void> {
  client.destroy();
}

/**
 * Main file of the program that launches the bot discord.
 */
(async () => {
  const client = new Client({
    intents: [
      Intents.FLAGS.GUILDS,
      Intents.FLAGS.GUILD_VOICE_STATES,
      Intents.FLAGS.GUILD_MEMBERS,
    ],
  });

  // Retrieval of available commands
  (client as any).commands = new Collection();
  const commandsData = await getCommands(
    `./${config.isProd ? "dist" : "src"}/commands`,
    "../../commands"
  );
  for (const command of commandsData.commands) {
    Logger.log(LoggerType.INFO, `Set command: ${command.data.name}`);
    (client as any).commands.set(command.data.name, command);
  }

  /**
   * Initialization at the beginning of the bot
   */
  client.once("ready", async () => {
    await sequelize.sync({ alter: true });

    EmbedFactory.init(client);

    checkLogs();
    setInterval(checkLogs, 24 * 60 * 60 * 1000); // Each day

    await checkSubscribers(client);
    setInterval(checkSubscribers, 15 * 60 * 1000, client); // Each 15 min

    Logger.log(LoggerType.INFO, "Bot initialization completed!");
  });

  /**
   * Manages interactions
   */
  client.on("interactionCreate", async (interaction) => {
    if (!interaction.isCommand()) return;

    const command = (client as any).commands.get(interaction.commandName);
    if (!command) return;

    try {
      Logger.logInteraction(interaction);
      await interaction.deferReply({ ephemeral: false });
      await command.execute(interaction);
    } catch (err) {
      Logger.log(
        LoggerType.ERROR,
        `Error during the execution of ${interaction.commandName}: ${err}`
      );
      const embed = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.ERROR,
        capitalizeFirstLetter(interaction.commandName),
        "There was an error while executing this command!"
      );
      await interaction.editReply({
        embeds: [embed],
      });
    }
  });

  if (config.isProd) {
    /**
     * Interception of errors to prevent the program from stopping
     */
    process.on("uncaughtException", async (err: any, origin: any) => {
      Logger.log(
        LoggerType.ERROR,
        `Error in ${origin}: ${err} => ${err.stack}`
      );
      await cleanStop(client);
      process.exit();
    });
  }

  /**
   * Interception of the SIGINT signal to inject a clean stop procedure
   */
  process.on("SIGINT", async () => {
    await cleanStop(client);
    process.exit();
  });

  /**
   * Interception of the SIGTERM signal to inject a clean stop procedure
   */
  process.on("SIGTERM", async () => {
    await cleanStop(client);
    process.exit();
  });

  // Starting the bot
  client.login(config.token);
})();
