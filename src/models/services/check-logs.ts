import { CustomLogger, Logger, LoggerType } from "../logger/logger";

/**
 * Deletes files that do not respect the retention rule
 */
export function checkLogs(): void {
  try {
    Logger.log(LoggerType.INFO, "Starting the checkLogs service...");
    const logger = new CustomLogger("./logs", 3);
    logger.clean();
  } catch (err) {
    Logger.log(LoggerType.WARN, `Error during checkLogs service: ${err}`);
  }
}
