import * as dayjs from "dayjs";
import { Client, MessageActionRow, MessageButton } from "discord.js";
import { AnimeSubscribes } from "../database/tables/anime-subscribes";
import { Logger, LoggerType } from "../logger/logger";
import fetch from "node-fetch";
import * as config from "../../config.json";
import {
  EmbedFactory,
  MessageEmbedColor,
} from "../managers/messageEmbed-factory";

interface NekoSamaEpisodeData {
  time: string;
  episode: string;
  title: string;
  url: string;
  anime_url: string;
  url_image: string;
  url_bg: string;
}

/**
 * Check if there is a new episode and contact the subscribed users
 * @param client The discord.js client
 * @returns Nothing
 */
export async function checkSubscribers(client: Client) {
  try {
    Logger.log(LoggerType.INFO, "Starting the checkSubscribers service...");

    let response;
    try {
      response = await fetch("https://nekosama.codexus.fr/api/episodes/today");
    } catch (error) {
      Logger.log(
        LoggerType.ERROR,
        `Service checkSubscribers > Failed to retrieve response: ${error}`
      );
    }

    if (!response || response.status !== 200) {
      const animeErrorAdminUser = await client.users.fetch(
        config.animeErrorAdminId
      );
      const embedError = EmbedFactory.generateMessageEmbed(
        MessageEmbedColor.ERROR,
        "🚨 nekosama.codexus.fr is ☠️",
        "Hello! \nI can't reach https://nekosama.codexus.fr/api/episodes/today \nIf you can fix it, can you help me?"
      );
      animeErrorAdminUser.send({ embeds: [embedError] });
      return;
    }

    const episodes_today = (await response.json()) as NekoSamaEpisodeData[];

    const subscribes = await AnimeSubscribes.findAll();
    const last_4_months = dayjs().subtract(4, "month");

    for (const subscribe of subscribes) {
      if (dayjs(subscribe.createdAt) < last_4_months) {
        await subscribe.destroy();
        continue;
      }

      for (const episode of episodes_today) {
        if (episode.title === subscribe.animeName) {
          const episodeNumber = parseInt(
            (episode.episode as string).replace("Ep. ", ""),
            10
          );

          if (episodeNumber > subscribe.latestEpisode) {
            const user = await client.users.fetch(subscribe.discordUserId);

            if (user) {
              const row = new MessageActionRow()
                .addComponents(
                  new MessageButton()
                    .setLabel("Watch the episode")
                    .setURL(`https://${config.animeDomainName}${episode.url}`)
                    .setStyle("LINK")
                )
                .addComponents(
                  new MessageButton()
                    .setLabel("See anime page")
                    .setURL(
                      `https://${config.animeDomainName}${episode.anime_url}`
                    )
                    .setStyle("LINK")
                );

              const embed = EmbedFactory.generateMessageEmbed(
                MessageEmbedColor.INFO,
                `New episode of ${episode.title}`,
                `Episode n°${episodeNumber}`
              );

              embed.setThumbnail(episode.url_image);

              await user.send({ embeds: [embed], components: [row] });
              Logger.log(
                LoggerType.INFO,
                `Service checkSubscribers > Notification of the user ${subscribe.discordUserId} for episode ${episodeNumber} of the anime ${subscribe.animeName}`
              );
            } else {
              Logger.log(
                LoggerType.ERROR,
                `Service checkSubscribers > Failed to fetch userID: ${subscribe.discordUserId}`
              );
            }

            subscribe.latestEpisode = episodeNumber;
            await subscribe.save();
          }
        }
      }
    }
  } catch (err) {
    Logger.log(
      LoggerType.WARN,
      `Error during checkSubscribers service: ${err}`
    );
  }
}
