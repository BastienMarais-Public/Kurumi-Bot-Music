import { Logger, LoggerType } from "../logger/logger";
import { customGifsManager } from "../../config.json";
import fetch from "node-fetch";

/**
 * Categories used by the Akaneko api
 */
export const enum AkanekoCategory {
  ASS = "ass",
  BDSM = "bdsm",
  BLOWJOB = "blowjob",
  CUM = "cum",
  DOUJIN = "doujin",
  FEET = "feet",
  FEMDOM = "femdom",
  FOXGIRL = "foxgirl",
  GLASSES = "glasses",
  HENTAI = "hentai",
  NETORARE = "netorare",
  MAID = "maid",
  MASTURBATION = "masturbation",
  ORGY = "orgy",
  PANTIES = "panties",
  PUSSY = "pussy",
  SCHOOL = "school",
  SUCCUBUS = "succubus",
  TENTACLES = "tentacles",
  THIGHS = "thighs",
  UGLYBASTARD = "uglyBastard",
  UNIFORM = "uniform",
  YURI = "yuri",
}

/**
 * Categories used by the Nekobot api
 */
export const enum NekobotCategory {
  HASS = "hass",
  HMIDRIFF = "hmidriff",
  PGIF = "pgif",
  H4K = "4k",
  HENTAI = "hentai",
  HOLO = "holo",
  HNEKO = "hneko",
  NEKO = "neko",
  HKITSUNE = "hkitsune",
  KEMONOMIMI = "kemonomimi",
  ANAL = "anal",
  HANAL = "hanal",
  GONEWILD = "gonewild",
  KANNA = "kanna",
  ASS = "ass",
  PUSSY = "pussy",
  THIGH = "thigh",
  HTHIGH = "hthigh",
  GAH = "gah",
  COFFEE = "coffee",
  FOOD = "food",
  PAIZURI = "paizuri",
  TENTACLE = "tentacle",
  BOOBS = "boobs",
  HBOOBS = "hboobs",
  YAOI = "yaoi",
}

const LIMIT_RETRY = 3;

/**
 * Manages the fetching of gifs and images
 */
class GifsManager {
  /**
   * Get a gif url from customGifsManager (https://gitlab.com/BastienMarais-Public/gifs-manager/)
   * @param category Desired image category
   * @param retry Number of tries
   * @returns The url of a gif
   */
  async customGifsManager(
    category: string,
    retry: number = 0
  ): Promise<string> {
    if (retry < LIMIT_RETRY) {
      try {
        const response = await fetch(
          `${customGifsManager}/categories/${category}/random`
        );
        const url = await response.json();
        if (!url || url === "") {
          return await Gifs.customGifsManager(category, retry + 1);
        }
        return url;
      } catch (err) {
        Logger.log(
          LoggerType.ERROR,
          `GifsManager > Error when retrieving a customGifsManager link: ${err}`
        );
      }
    }
    return "";
  }

  /**
   * Get a ecchi gif/image url from nekos.life
   * @param retry Number of tries
   * @returns The url of a gif/image
   */
  async ecchi(retry: number = 0): Promise<string> {
    if (retry < LIMIT_RETRY) {
      try {
        const response = await (
          await fetch("https://nekos.life/api/v2/img/lewd")
        ).json();
        if (!response.url || response.url === "") {
          return await Gifs.ecchi(retry + 1);
        }
        return response.url;
      } catch (err) {
        Logger.log(
          LoggerType.ERROR,
          `GifsManager > Error when retrieving a nekos.life link: ${err}`
        );
      }
    }
    return "";
  }

  /**
   * Get a gif/image url from nekobot.xyz
   * @param type Desired image type
   * @param retry Number of tries
   * @returns The url of a gif/image
   */
  async nekobot(type: NekobotCategory, retry: number = 0): Promise<string> {
    if (retry < LIMIT_RETRY) {
      try {
        const response = await (
          await fetch(`https://nekobot.xyz/api/image?type=${type}`)
        ).json();
        if (!response.message || response.message === "") {
          return await Gifs.nekobot(type, retry + 1);
        }
        return response.message;
      } catch (err) {
        Logger.log(
          LoggerType.ERROR,
          `GifsManager > Error when retrieving a nekobot.xyz link: ${err}`
        );
      }
    }
    return "";
  }

  /**
   * Get a gif/image url from akaneko-api.herokuapp.com => http://51.161.33.197/
   * @param genre Desired image category
   * @param retry Number of tries
   * @returns The url of a gif/image
   */
  async akaneko(genre: AkanekoCategory, retry: number = 0): Promise<string> {
    if (retry < LIMIT_RETRY) {
      try {
        const response = await (
          await fetch(`http://51.161.33.197/api/${genre}`)
        ).json();

        if (!response.url || response.url === "") {
          return await Gifs.akaneko(genre, retry + 1);
        }
        return response.url;
      } catch (err) {
        Logger.log(
          LoggerType.ERROR,
          `GifsManager > Error when retrieving the akaneko link: ${err}`
        );
      }
    }
    return "";
  }
}

const Gifs = new GifsManager();
export { Gifs };
