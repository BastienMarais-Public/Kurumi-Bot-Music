import { CommandInteraction } from "discord.js";
import { Sequelize } from "sequelize-typescript";
import { Statistics } from "../database/tables/statistics";

/**
 * Represents a reduced element of statistics
 */
export interface StatisticsData {
  command: string;
  option: string;
  use: number;
}

/**
 * Manages statistics of the bot
 */
class StatisticsManager {
  /**
   * Allows you to update the statistics of a given interaction
   * @param interaction Interaction that must be recorded in the statistics
   * @param optionKey Key of the string option to be recorded
   */
  async updateStatistics(
    interaction: CommandInteraction,
    optionKey?: string
  ): Promise<void> {
    const guildId = interaction.guildId;
    const commandName = interaction.commandName;
    let option = "";
    if (optionKey && optionKey !== "") {
      option = interaction.options.getString(optionKey) || "";
    }

    const filter = {
      where: { guildId: guildId, command: commandName, option: option },
    };

    const data = await Statistics.findOne(filter);
    if (data) {
      data.use += 1;
      data.save();
    } else {
      Statistics.create({
        guildId: guildId,
        command: commandName,
        option: option,
        use: 1,
      });
    }
  }

  /**
   * Returns for each command all its usage for all guilds
   * @returns The usage list for each of the commands
   */
  async getGlobalTopStatistics(): Promise<StatisticsData[]> {
    const result: StatisticsData[] = [];
    const data = await Statistics.findAll({
      attributes: [
        "command",
        "option",
        [Sequelize.fn("sum", Sequelize.col("Statistics.use")), "use"],
      ],
      group: ["command", "option"],
      order: [["use", "DESC"]],
    });
    const servers: string[] = [];
    const guilds = await Statistics.findAll({ attributes: ["guildId"] });
    for (const row of guilds) {
      if (!servers.includes(row.guildId)) servers.push(row.guildId);
    }
    result.push({ command: "No. of servers", option: "", use: servers.length });
    for (const row of data) {
      result.push({ command: row.command, option: row.option, use: row.use });
    }

    return result;
  }

  /**
   * Returns for each command all its usage for the given guild
   * @param guildId The guild id that will be used as a filter
   * @returns The usage list for each of the commands of the guild
   */
  async getLocalTopStatistics(guildId: string): Promise<StatisticsData[]> {
    const result: StatisticsData[] = [];
    const data = await Statistics.findAll({
      where: { guildId: guildId },
      order: [["use", "DESC"]],
    });

    for (const row of data) {
      result.push({ command: row.command, option: row.option, use: row.use });
    }

    return result;
  }
}

const Stats = new StatisticsManager();
export { Stats };
