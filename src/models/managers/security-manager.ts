import {
  CommandInteraction,
  GuildMember,
  TextChannel,
  VoiceBasedChannel,
} from "discord.js";
import { Logger, LoggerType } from "../logger/logger";
import * as config from "../../config.json";

/**
 * Regroups the different security tests for the bots' commands
 */
class SecurityManager {
  /**
   * Get the GuildMember that caused the interaction
   * @param interaction The interaction of the command
   * @returns The Guild member or null
   */
  async getGuildMemberByInteraction(
    interaction: CommandInteraction
  ): Promise<GuildMember | null> {
    if (interaction.guild) {
      return await interaction.guild.members.fetch(interaction.user.id);
    }
    return null;
  }

  /**
   * Get the VoiceChannel that caused the interaction
   * @param interaction The interaction of the command
   * @returns The voice channel or null
   */
  async getVoiceChannelByInteraction(
    interaction: CommandInteraction
  ): Promise<VoiceBasedChannel | null> {
    const member = await this.getGuildMemberByInteraction(interaction);
    if (member) {
      return member.voice.channel;
    }
    return null;
  }

  /**
   * Checks if the given user or the user who initiated the interaction has the desired permission
   * @param memberOrInteraction Guild member or interaction to be verified
   * @param permission
   * @returns True or false
   */
  async hasPermission(
    memberOrInteraction: GuildMember | CommandInteraction,
    permission: string
  ): Promise<boolean> {
    try {
      let member: GuildMember | null = null;
      if (memberOrInteraction instanceof CommandInteraction) {
        member = await this.getGuildMemberByInteraction(memberOrInteraction);
      }
      if (memberOrInteraction instanceof GuildMember)
        member = memberOrInteraction;
      if (member) {
        for (const perm of member.permissions) {
          if (perm === permission) return true;
        }
      }
      return false;
    } catch (err) {
      Logger.log(
        LoggerType.ERROR,
        `Error during Security.hasPermission: ${err}`
      );
      return false;
    }
  }

  /**
   * Checks if the given user or the initiator of the interaction is in a voice channel
   * @param memberOrInteraction Guild member or interaction to be verified
   * @returns True or false
   */
  async useVoiceChannel(
    memberOrInteraction: GuildMember | CommandInteraction
  ): Promise<boolean> {
    let member: GuildMember | null = null;
    if (memberOrInteraction instanceof CommandInteraction) {
      member = await this.getGuildMemberByInteraction(memberOrInteraction);
    }
    if (memberOrInteraction instanceof GuildMember)
      member = memberOrInteraction;

    if (member) {
      if (member.voice.channel) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if the user who initiated the interaction is the bot owner
   * @param interaction The interaction of the command
   * @returns True or false
   */
  isBotOwner(interaction: CommandInteraction): boolean {
    return interaction.user.id === config.ownerId;
  }

  /**
   * Checks if the user who initiated the interaction is the guild owner
   * @param interaction The interaction of the command
   * @returns True or false
   */
  isGuildOwner(interaction: CommandInteraction): boolean {
    if (interaction.guild) {
      return interaction.guild.ownerId === interaction.user.id;
    }
    return false;
  }

  /**
   * Checks if the channel is NSFW or not
   * @param channelOrInteraction Text Channel or interaction to check
   * @returns True or false
   */
  isNSFW(channelOrInteraction: TextChannel | CommandInteraction): boolean {
    let channel = null;

    if (channelOrInteraction instanceof TextChannel)
      channel = channelOrInteraction;
    if (
      channelOrInteraction instanceof CommandInteraction &&
      channelOrInteraction.channel
    )
      channel = channelOrInteraction.channel as TextChannel;

    if (channel) return channel.nsfw;
    return false;
  }
}

const Security = new SecurityManager();
export { Security };
