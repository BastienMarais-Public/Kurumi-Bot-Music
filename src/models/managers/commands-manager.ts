import * as fs from "fs";

export interface CommandsData {
  commands: any[];
  json: any[];
}

/**
 * Retrieves the list of sub-folders of the given path
 * @param path Path of the folder to search
 * @returns The list of sub-folders
 */
export function getDirectories(path: string): string[] {
  return fs.readdirSync(path).filter(function (file) {
    return fs.statSync(path + "/" + file).isDirectory();
  });
}

/**
 * Generates a CommandsData allowing the recording of orders and the use in the client
 * @param root Path from the root of the project
 * @param relative Path from the declaration of this function
 * @returns A commandsData with all the information
 */
export async function getCommands(
  root: string,
  relative: string
): Promise<CommandsData> {
  const commands: any[] = [];
  const commandsJSON: any[] = [];
  const commandFolders = getDirectories(root);

  for (const folder of commandFolders) {
    const commandFiles = fs
      .readdirSync(`${root}/${folder}`)
      .filter((file) => file.endsWith(".ts") || file.endsWith(".js"));
    for (const file of commandFiles) {
      const command = await import(`${relative}/${folder}/${file}`);
      commands.push(command);
      commandsJSON.push(command.data.toJSON());
    }
  }
  return { commands: commands, json: commandsJSON };
}
