import { Client } from "discord.js";

import { MessageEmbed, EmbedAuthorData, EmbedFooterData } from "discord.js";

/**
 * Available colors for MessageEmbed
 */
export const enum MessageEmbedColor {
  SUCCESS = "#12C41B",
  WARNING = "#C47A12",
  ERROR = "#C42712",
  INFO = "#0099FF",
}

interface MessageEmbedFactoryInterface {
  username: string;
  avatar: string;
  footer: string;
  id: string;
  init(client: Client): void;
  generateMessageEmbed(
    color: MessageEmbedColor,
    title: string,
    description: string
  ): MessageEmbed;
}

/**
 * Allows to generate preconfigured MessageEmbed
 */
class MessageEmbedFactory implements MessageEmbedFactoryInterface {
  username: string;
  avatar: string;
  footer: string;
  id: string;

  /**
   * Default constructor
   */
  constructor() {
    this.username = "";
    this.avatar = "";
    this.footer = "";
    this.id = "";
  }

  /**
   * Initialize the factory with the client's data
   * @param client Discord.js client
   */
  init(client: Client): void {
    this.username = client.user?.username || "";
    this.id = client.user?.id || "";
    this.avatar = client.user?.avatarURL() || "";
    this.footer = `Thank you for using ${this.username} 🥰`;
  }

  /**
   * Generates a preconfigured MessageEmbed
   * @param color Color of the MessageEmbed
   * @param title Title of the MessageEmbed
   * @param description Description of the MessageEmbed
   * @returns Preconfigured MessageEmbed
   */
  generateMessageEmbed(
    color: MessageEmbedColor,
    title: string,
    description: string
  ): MessageEmbed {
    const author: EmbedAuthorData = {
      name: this.username,
      url: this.avatar,
      iconURL: this.avatar,
    };

    const footerData: EmbedFooterData = {
      text: this.footer,
      iconURL: this.avatar,
    };

    const embed = new MessageEmbed()
      .setColor(color)
      .setTitle(title)
      .setAuthor(author);

    if (description) {
      embed.setDescription(description);
    }

    embed.setThumbnail(this.avatar);
    embed.setFooter(footerData);
    embed.setTimestamp();

    return embed;
  }
}

const EmbedFactory = new MessageEmbedFactory();
export { EmbedFactory };
