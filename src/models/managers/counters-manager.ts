import { Counters } from "../database/tables/counters";

/**
 * Represents a reduced element of counters
 */
interface CountersData {
  name: string;
  target: string;
  value: number;
}

/**
 * Manages the counters of the bot
 */
class CountersManager {
  /**
   * Allows you to add a given value to the requested counter
   * @param guildId The id of the guild that uses the counter
   * @param name The name of the counter
   * @param target User who will act as a filter
   * @param valueToAdd The value to be added (1 or -1)
   */
  async addValueToCounter(
    guildId: string,
    name: string,
    target: string,
    valueToAdd: number
  ): Promise<void> {
    const filter = {
      where: { guildId: guildId, name: name, target: target },
    };

    const data = await Counters.findOne(filter);
    if (data) {
      if (data.value + valueToAdd < 0) data.value = 0;
      else data.value += valueToAdd;

      data.save();
    } else {
      if (valueToAdd > 0) {
        Counters.create({
          guildId: guildId,
          name: name,
          target: target,
          value: valueToAdd,
        });
      } else {
        Counters.create({
          guildId: guildId,
          name: name,
          target: target,
          value: 0,
        });
      }
    }
  }

  /**
   * Returns the information of a requested counter and user
   * @param guildId The guild id that will be used as a filter
   * @param name Name of the counter on which we filter
   * @param target User who will act as a filter
   * @returns The data of a counter
   */
  async getCounter(
    guildId: string,
    name: string,
    target: string
  ): Promise<CountersData> {
    const data = await Counters.findOne({
      where: { guildId: guildId, name: name, target: target },
    });
    return {
      name: data?.name || name,
      target: data?.target || target,
      value: data?.value || 0,
    };
  }

  /**
   * Returns the counters of a user on a given server
   * @param guildId The guild id that will be used as a filter
   * @param target User who will act as a filter
   * @returns List of counters
   */
  async getAllCounters(
    guildId: string,
    target: string
  ): Promise<CountersData[]> {
    const result: CountersData[] = [];
    const data = await Counters.findAll({
      where: { guildId: guildId, target: target },
      order: [["value", "DESC"]],
    });

    for (const row of data) {
      result.push(await Counter.getCounter(guildId, row.name, row.target));
    }
    return result;
  }

  /**
   * Returns a top of the people of a counter on a given server
   * @param guildId The guild id that will be used as a filter
   * @param name Name of the counter on which we filter
   * @param limit Maximum number of items in top
   * @returns List of counters
   */
  async getTopCounters(
    guildId: string,
    name: string,
    limit: number
  ): Promise<CountersData[]> {
    const result: CountersData[] = [];
    const data = await Counters.findAll({
      where: { guildId: guildId, name: name },
      order: [["value", "DESC"]],
    });

    let i = 0;
    for (const row of data) {
      if (limit === i) break;
      result.push(await Counter.getCounter(guildId, row.name, row.target));
      i += 1;
    }
    return result;
  }
}

const Counter = new CountersManager();
export { Counter };
