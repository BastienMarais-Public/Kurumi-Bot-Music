import { Column, Model, Table } from "sequelize-typescript";

@Table
export class Statistics extends Model {
  @Column
  guildId!: string;

  @Column
  command!: string;

  @Column
  option!: string;

  @Column
  use!: number;
}
