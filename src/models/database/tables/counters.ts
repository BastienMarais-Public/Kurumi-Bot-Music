import { Column, Model, Table } from "sequelize-typescript";

@Table
export class Counters extends Model {
  @Column
  guildId!: string;

  @Column
  name!: string;

  @Column
  target!: string;

  @Column
  value!: number;
}
