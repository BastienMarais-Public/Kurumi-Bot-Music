import { Column, Model, Table } from "sequelize-typescript";

@Table
export class AnimeSubscribes extends Model {
  @Column
  animeUrl!: string;

  @Column
  animeName!: string;

  @Column
  discordUserId!: string;

  @Column
  latestEpisode!: number;
}
