/**
 * Returns the string with the first letter capitalized
 * @param str The string that needs to be transformed
 * @returns The string with the first letter in upper case
 */
export function capitalizeFirstLetter(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
