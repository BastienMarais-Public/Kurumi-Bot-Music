import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import * as config from "./config.json";
import { Logger, LoggerType } from "./models/logger/logger";

/**
 * Allows to remove all the bot's commands on the prod
 */
(() => {
  const rest = new REST({ version: "9" }).setToken(config.token);

  rest.get(Routes.applicationCommands(config.clientId)).then((data: any) => {
    const promises = [];
    for (const command of data) {
      const deleteUrl: any = `${Routes.applicationCommands(config.clientId)}/${
        command.id
      }`;
      Logger.log(
        LoggerType.INFO,
        `Deleting the global command: ${command.name}`
      );
      promises.push(rest.delete(deleteUrl));
    }
    return Promise.all(promises);
  });
})();
