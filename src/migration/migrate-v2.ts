import { sequelize } from "../models/database/database";
import { Counters } from "../models/database/tables/counters";
import { Statistics } from "../models/database/tables/statistics";
import { Logger, LoggerType } from "../models/logger/logger";
import * as counters from "./data/counters-ban.json";
import * as stats from "./data/stats.json";

(async () => {
  await sequelize.sync({ alter: true });

  /***********************************************
   * STATS
   ***********************************************/
  const mapping = new Map([
    ["help", { command: "help", option: "" }],
    ["about", { command: "about", option: "" }],
    ["send-all", { command: "send-all", option: "" }],
    ["stats-global", { command: "stats", option: "global" }],
    ["stats", { command: "stats", option: "local" }],
    ["add-role", { command: "role-add", option: "" }],
    ["remove-role", { command: "role-remove", option: "" }],
    ["clear-msg", { command: "clear", option: "" }],
    ["neko-last", { command: "neko-last", option: "" }],
    ["neko-search", { command: "neko-search", option: "" }],
    ["add-ban", { command: "counter-add", option: "ban" }],
    ["get-ban", { command: "counter-get", option: "ban" }],
    ["remove-ban", { command: "counter-minus", option: "ban" }],
    ["angry", { command: "gif", option: "angry" }],
    ["blush", { command: "gif", option: "blush" }],
    ["cuddle", { command: "gif", option: "cuddle" }],
    ["dance", { command: "gif", option: "dance" }],
    ["fight", { command: "gif", option: "fight" }],
    ["gif", null],
    ["hug", { command: "gif", option: "hug" }],
    ["kick", { command: "gif", option: "kick" }],
    ["kiss", { command: "gif", option: "kiss" }],
    ["kurumi", { command: "gif", option: "kurumi" }],
    ["neko", { command: "gif", option: "neko" }],
    ["panda", { command: "gif", option: "panda" }],
    ["pat", { command: "gif", option: "pat" }],
    ["poke", { command: "gif", option: "poke" }],
    ["sad", { command: "gif", option: "sad" }],
    ["slap", { command: "gif", option: "slap" }],
    ["tickle", { command: "gif", option: "tickle" }],
    ["waifu", { command: "gif", option: "waifu" }],
    ["current", { command: "current", option: "" }],
    ["disconnect", { command: "disconnect", option: "" }],
    ["join", { command: "join", option: "" }],
    ["loop-queue", null],
    ["loop", { command: "loop", option: "" }],
    ["pause", { command: "pause", option: "" }],
    ["play", { command: "play", option: "" }],
    ["queue", { command: "queue", option: "" }],
    ["resume", { command: "pause", option: "" }],
    ["skip", { command: "skip", option: "" }],
    ["ecchi", { command: "nsfw", option: "ecchi" }],
    ["hentai", { command: "nsfw", option: "hentai" }],
    ["succubus", { command: "nsfw", option: "succubus" }],
    ["tentacles", { command: "nsfw", option: "tentacles" }],
    ["yuri", { command: "nsfw", option: "yuri" }],
  ]);

  interface OldStats {
    id: string;
    commands: any;
  }

  for (const guild of stats as OldStats[]) {
    const guildId = guild.id;
    const commands = guild.commands;
    for (const key of mapping.keys()) {
      const use = commands[key];
      if (use) {
        const result = mapping.get(key);
        if (result) {
          Logger.log(
            LoggerType.INFO,
            `[${guildId}] Migration of statistic ${key} ==> ${result.command}${
              result.option === "" ? "" : ` > ${result.option}`
            }, use: ${use}`
          );

          const filter = {
            where: {
              guildId: guildId,
              command: result.command,
              option: result.option,
            },
          };

          const data = await Statistics.findOne(filter);
          if (data) {
            data.use += use;
            data.save();
          } else {
            Statistics.create({
              guildId: guildId,
              command: result.command,
              option: result.option,
              use: use,
            });
          }
        }
      }
    }
  }

  /***********************************************
   * COUNTERS BAN
   ***********************************************/
  const name = "ban";
  for (const guild of counters as OldStats[]) {
    const guildId = guild.id;
    const users = guild.commands;
    for (const key of Object.keys(users)) {
      const value = users[key];
      if (value) {
        Logger.log(
          LoggerType.INFO,
          `[${guildId}] Migration counter ${name} of ${key} ==> ${value}`
        );

        const filter = {
          where: { guildId: guildId, name: name, target: key },
        };

        const data = await Counters.findOne(filter);
        if (data) {
          data.value += value;
          data.save();
        } else {
          Counters.create({
            guildId: guildId,
            name: name,
            target: key,
            value: value,
          });
        }
      }
    }
  }
})();
