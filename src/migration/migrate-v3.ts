import { sequelize } from "../models/database/database";
import { Statistics } from "../models/database/tables/statistics";
import { Logger, LoggerType } from "../models/logger/logger";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import * as config from "../config.json";

(async () => {
  await sequelize.sync({ alter: true });

  /***********************************************
   * Renamed commands
   ***********************************************/
  const renamedCommands = [
    ["neko-last", "anime-last"],
    ["neko-list", "anime-list"],
    ["neko-search", "anime-search"],
    ["neko-subscribe", "anime-subscribe"],
    ["neko-unsubscribe", "anime-unsubscribe"],
  ];

  renamedCommands.forEach(async (record) => {
    const oldName = record[0];
    const newName = record[1];
    Logger.log(
      LoggerType.INFO,
      `Database renaming of command "${oldName}" to "${newName}"`
    );
    await Statistics.update(
      { command: newName },
      {
        where: {
          command: oldName,
        },
      }
    );
  });

  /***********************************************
   * Unpublish commands
   ***********************************************/
  const commandNameList = [
    "neko-last",
    "neko-list",
    "neko-search",
    "neko-subscribe",
    "neko-unsubscribe",
  ];
  const rest = new REST({ version: "9" }).setToken(config.token);

  // Dev commands
  for (const guildId of config.guildId) {
    rest
      .get(Routes.applicationGuildCommands(config.clientId, guildId))
      .then(async (data: any) => {
        const promises = [];
        for (const command of data) {
          if (commandNameList.includes(command.name)) {
            const deleteUrl: any = `${Routes.applicationGuildCommands(
              config.clientId,
              guildId
            )}/${command.id}`;
            Logger.log(
              LoggerType.INFO,
              `[${guildId}] Deleting the command: ${command.name}`
            );
            promises.push(rest.delete(deleteUrl));
          }
        }
        await Promise.all(promises);
      });
  }

  // Global commands
  rest
    .get(Routes.applicationCommands(config.clientId))
    .then(async (data: any) => {
      const promises = [];
      for (const command of data) {
        if (commandNameList.includes(command.name)) {
          const deleteUrl: any = `${Routes.applicationCommands(
            config.clientId
          )}/${command.id}`;
          Logger.log(
            LoggerType.INFO,
            `Deleting the global command: ${command.name}`
          );
          promises.push(rest.delete(deleteUrl));
        }
      }
      await Promise.all(promises);
    });
})();
